import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import javax.imageio.ImageIO;
import javax.swing.*;

public class ScreenMe extends JFrame {
    private JPanel pixelPanel;
    private int pixelSize = 25;
    private int numPixels = 20;
    private Random random = new Random();

    public ScreenMe() {
        super("Pixel Art Generator");
        setSize(numPixels * pixelSize, numPixels * pixelSize);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        setLayout(new BorderLayout());
        pixelPanel = new PixelPanel();
        add(pixelPanel, BorderLayout.CENTER);
        pixelPanel.requestFocus();
        pixelPanel.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_F11) {
                    saveScreenshot();
                }
            }
        });
    }

    private void saveScreenshot() {
        try {
            BufferedImage screenshot = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_RGB);
            Graphics2D g2d = screenshot.createGraphics();
            paint(g2d);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
            String filename = sdf.format(new Date()) + ".png";
            ImageIO.write(screenshot, "png", new File(filename));
            JOptionPane.showMessageDialog(this, "Screenshot saved as " + filename);
        } catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this, "Error saving screenshot: " + ex.getMessage());
        }
    }

    class PixelPanel extends JPanel {
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            for (int i = 0; i < numPixels; i++) {
                for (int j = 0; j < numPixels; j++) {
                    if ((i + j) % 2 == 0) {
                        g.setColor(Color.BLACK);
                    } else {
                        g.setColor(new Color(226, 221, 179)); // sand color
                    }
                    g.fillRect(i * pixelSize, j * pixelSize, pixelSize, pixelSize);
                }
            }
        }
    }

    public static void main(String[] args) {
        new ScreenMe();
    }
}
